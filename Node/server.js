const express = require('express');
const connectDB = require('./DB/Connection')
const app = express();
const cors = require('cors');
connectDB();
const router = require('./routes/User');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
const Port = process.env.Port || 3000;
app.use('/api/User',router);
app.listen(Port,()=> console.log('Server started'))