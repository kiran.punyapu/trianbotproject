const mongoose = require('mongoose');

const user = new mongoose.Schema({
    fullName:{
        type:String
    },
    gender:{
        type:String
    },
    mobileNumber:{
        type:Number
    }
});

module.exports = User = mongoose.model('user',user);