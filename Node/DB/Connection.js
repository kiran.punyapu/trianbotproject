const mongoose = require('mongoose');

const URI = 'mongodb+srv://dbUser:dbUser@cluster0.dgbjt.mongodb.net/<dbname>?retryWrites=true&w=majority';

const connectdb = async() =>{
    await mongoose.connect(URI,{ useUnifiedTopology: true, useNewUrlParser: true});
    console.log('db connected..!')
}

module.exports = connectdb