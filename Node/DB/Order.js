const mongoose = require('mongoose');

const order = new mongoose.Schema({
    userId:{
        type:Number
    },
    orderName:{
        type:String
    }
});

module.exports = Order = mongoose.model('order',order);