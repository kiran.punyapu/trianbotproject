const express = require('express');
const router = express.Router();
const User = require('../DB/User');
const Order = require('../DB/Order')

router.post('/crateuser',async (req,res)=>{
    const response = await User.create(req.body);
    res.status(200).send(response);
});

router.post('/crateorder',async (req,res)=>{
    const response = await Order.create(req.body);
    res.status(200).send(response);
});

router.get('/getusers',async (req,res)=>{
    res.status(200).send(await User.find())
});

router.get('/order/:userId',async(req,res)=>{
    res.status(200).send(await Order.find({userId:req.params.userId}))
 });

module.exports = router